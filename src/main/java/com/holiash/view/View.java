package com.holiash.view;

import com.holiash.controller.ReflectionWorkerController;
import com.holiash.util.Command;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class View {

  private Map<Integer, Command> menu;
  private List<String> menuList;
  private ReflectionWorkerController controller;

  public View(ReflectionWorkerController controller) {
    this.controller = controller;
    menu = new HashMap<>();
    menuList = new ArrayList<>();
    menu.put(1, controller::printAnnotatedFields);
    menu.put(2, controller::invokeMethods);
    menu.put(3, controller::setValue);
    menuList.add("1 - Print annotated fields");
    menuList.add("2 - Invoke methods");
    menuList.add("3 - Set value");
    menuList.add("9 - Exit");
  }

  public void showMenu() {
    menuList.forEach(System.out::println);
  }

  public void execute(int num) {
    if (menu.containsKey(num)) {
      menu.get(num).execute();
    } else {
      System.out.println("Wrong value");
    }
  }
}
