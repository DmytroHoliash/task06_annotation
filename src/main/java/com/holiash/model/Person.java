package com.holiash.model;

import com.holiash.annotation.MyAnno;

public class Person {

  @MyAnno()
  private String name;
  @MyAnno(name = "MyName", age = 20)
  private int age;
  private String address;

  public Person() {
    this("None", 0, "None");
  }

  public Person(String name, int age, String address) {
    this.name = name;
    this.age = age;
    this.address = address;
  }

  public int someMethod() {
    System.out.println("some method without params");
    return 1;
  }

  public void someMethod(String msg) {
    System.out.println("Some method with string param = " + msg);
  }

  public double someMethod(int... arr) {
    System.out.println("Some method with int[] params: ");
    for (int a : arr) {
      System.out.print(a + "\t");
    }
    System.out.println();
    return 0.25;
  }

  public void myMethod(String... strings) {
    System.out.println("my method with string[] param: ");
    for (String s : strings) {
      System.out.print(s + "; ");
    }
    System.out.println();
  }

  public void myMethod(String a, int... args) {
    System.out.println("my method with string param: " + a + " and int[] params");
    for (int i : args) {
      System.out.print(i + "\t");
    }
    System.out.println();
  }

  @Override
  public String toString() {
    return "Person{" +
        "name='" + name + '\'' +
        ", age=" + age +
        ", address='" + address + '\'' +
        '}';
  }
}
