package com.holiash.model;

import static com.holiash.util.MyLogger.logger;

import com.holiash.annotation.MyAnno;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionWorker<T> {

  private T obj;
  private Class<T> tClass;

  public ReflectionWorker(Class<T> tClass) {
    logger.info("creating ReflectionWorker");
    this.tClass = tClass;
    try {
      obj = tClass.getConstructor(String.class, int.class, String.class)
          .newInstance("Dmytro", 19, "Lviv");
    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  public void printAnnotatedFields() {
    logger.info("Printing annotated fields");
    Field[] fields = tClass.getDeclaredFields();
    for (Field f : fields) {
      if (f.isAnnotationPresent(MyAnno.class)) {
        System.out.println("Field name = " + f.getName());
        System.out.println("Field annotation = " + f.getAnnotation(MyAnno.class));
      }
    }
  }

  public void invokeMethods() {
    logger.info("Invoking methods");
    String[] args = new String[]{"one", "two"};
    int[] values = new int[]{1, 2, 5};
    try {
      Method m1 = tClass.getDeclaredMethod("someMethod");
      Method m2 = tClass.getDeclaredMethod("someMethod", String.class);
      Method m3 = tClass.getDeclaredMethod("someMethod", int[].class);
      Method m4 = tClass.getDeclaredMethod("myMethod", String[].class);
      Method m5 = tClass.getDeclaredMethod("myMethod", String.class, int[].class);
      m1.invoke(obj);
      m2.invoke(obj, "Hello");
      m3.invoke(obj, values);
      m4.invoke(obj, (Object) args);
      m5.invoke(obj, "Hello", values);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
    }
  }

  public void setValue() {
    try {
      Field field = tClass.getDeclaredField("age");
      field.setAccessible(true);
      if (field.getType() == int.class) {
        field.setInt(obj, 15);
      }
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
    System.out.println(obj);
  }

}
