package com.holiash.util;

@FunctionalInterface
public interface Command {

  void execute();
}
