package com.holiash.util;

import java.util.ResourceBundle;

public class Const {
  private static final ResourceBundle bundle = ResourceBundle.getBundle("consts");
  public static final int EXIT_CODE = Integer.parseInt(bundle.getString("exit_code"));
}
