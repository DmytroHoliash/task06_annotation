package com.holiash.controller;

import com.holiash.model.ReflectionWorker;

public class ReflectionWorkerController {
  private ReflectionWorker worker;

  public ReflectionWorkerController(ReflectionWorker worker) {
    this.worker = worker;
  }

  public void printAnnotatedFields() {
    worker.printAnnotatedFields();
  }

  public void invokeMethods() {
    worker.invokeMethods();
  }

  public void setValue() {
    worker.setValue();
  }
}
