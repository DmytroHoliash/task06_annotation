package com.holiash;

import static com.holiash.util.Const.EXIT_CODE;

import com.holiash.controller.ReflectionWorkerController;
import com.holiash.model.Person;
import com.holiash.model.ReflectionWorker;
import com.holiash.view.View;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
  private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
  public static void main(String[] args) throws IOException {
    ReflectionWorker<Person> worker = new ReflectionWorker<>(Person.class);
    ReflectionWorkerController controller = new ReflectionWorkerController(worker);
    View view = new View(controller);
    int choice;
    while (true) {
      view.showMenu();
      choice = Integer.parseInt(br.readLine());
      if (choice == EXIT_CODE) break;
      view.execute(choice);
    }
  }
}
